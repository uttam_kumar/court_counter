package com.example2.hp.courtcounter;

import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity {
    int a = 0;
    int b = 0;
    int count1 = 0;
    int count2 = 0;
    TextView txt_vA, txt_vB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_vA = findViewById(R.id.team_A);
        txt_vB = findViewById(R.id.team_B);


    }

    public void points3TeamA(View view) {
        a = 3;
        b = 1;
        display(a, b);
    }

    public void points3TeamB(View view) {
        a = 3;
        b = 2;
        display(a, b);
    }

    public void points2TeamA(View view) {
        a = 2;
        b = 1;
        display(a, b);
    }

    public void points2TeamB(View view) {
        a = 2;
        b = 2;
        display(a, b);
    }

    public void pointFreeTeamA(View view) {
        a = 1;
        b = 1;
        display(a, b);
    }

    public void pointFreeTeamB(View view) {
        a = 1;
        b = 2;
        display(a, b);
    }

    public void reset(View view) {
        a = 0;
        b = 0;
        display(a , b);
    }

    private void display(int num1, int num2) {



        if (num1 == 3 && num2 == 1) {
            count1 = count1 + 3;
            if(count1>32)
                count1=32;
            txt_vA.setText("" + count1);
        } else if (num1 == 3 && num2 == 2) {
            count2 = count2 + 3;
            if(count2>32)
                count2=32;
            txt_vB.setText("" + count2);
        }else if (num1 == 2 && num2 == 1) {
            count1 = count1 + 2;
            if(count1>32)
                count1=32;
            txt_vA.setText("" + count1);
        } else if (num1 == 2 && num2 == 2) {
            count2 = count2 + 2;
            if(count2>32)
                count2=32;
            txt_vB.setText("" + count2);
        }else if (num1 == 1 && num2 == 1) {
            count1 = count1 + 1;
            if(count1>32)
                count1=32;
            txt_vA.setText("" + count1);
        } else if (num1 == 1 && num2 == 2) {
            count2 = count2 + 1;
            if(count2>32)
                count2=32;
            txt_vB.setText("" + count2);
        }else if (num1 == 0 && num2 == 0) {
            count2 = 0;
            count1 = 0;
            txt_vA.setText(""+count1);
            txt_vB.setText(""+count2);
        }

        if(count1==32){
            showMessageOKCancel("WIN: Team A",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startAgain();
                        }
                    });

        }else if(count2==32){

            showMessageOKCancel("WIN: Team B",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startAgain();
                        }
                    });
        }

    }



    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();

    }

    private void startAgain(){
        count2 = 0;
        count1 = 0;
        txt_vA.setText(""+count1);
        txt_vB.setText(""+count2);
    }
}